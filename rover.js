function calculateRoverPath(map) {
	const fs = require("fs");
	let n = map[0].length - 1;
	let m = map.length - 1;
	let nO = 0;
	let mO = 0;
	let path = [`[0][0]->`];
	let fuelArr = [map[mO][nO]];

	while (mO < m && nO < n) {
		if (
			Math.min(
				Math.abs(map[mO][nO] - map[mO][nO + 1]),
				Math.abs(map[mO][nO] - map[mO + 1][nO])
			) === Math.abs(map[mO][nO + 1] - map[mO][nO]) &&
			Math.min(
				Math.abs(map[mO][nO] - map[mO][nO + 1]),
				Math.abs(map[mO][nO] - map[mO + 1][nO])
			) != 0
		) {
			nO++;
		} else if (
			Math.min(
				Math.abs(map[mO][nO] - map[mO][nO + 1]),
				Math.abs(map[mO][nO] - map[mO + 1][nO])
			) === Math.abs(map[mO + 1][nO] - map[mO][nO])
		) {
			mO++;
		} else {
			nO++;
		}

		path.push(`[${mO}][${nO}]->`);

		if (mO === m) {
			while (nO != n - 1) {
				nO++;
				path.push(`[${mO}][${nO}]->`);
			}
		}

		if (nO === n) {
			while (mO != m - 1) {
				mO++;
				path.push(`[${mO}][${nO}]->`);
			}
		}

		fuelArr.push(map[mO][nO]);
	}

	path.push(`[${m}][${n}]`);
	fuelArr.push(map[m][n]);

	let steps = path.length - 1;
	let fuel = path.length - 1;

	for (let i = 0; i < fuelArr.length - 1; i++) {
		fuel += Math.abs(fuelArr[i] - fuelArr[i + 1]);
	}

	path = path.join().replace(/,/g, "");

	let roverPath = {
		path: path,
		fuel: fuel,
		steps: steps,
	};

	fs.appendFileSync(
		"path-plan.txt",
		`${roverPath.path}\nsteps : ${roverPath.steps}\nfuel: ${roverPath.fuel}`,
		"ascii"
	);

	let angularSteps = m + n;
	let angularPath = [];
	let m0 = 0;
	let n0 = 0;
	let pathAngular = [];

	while (n0 < n) {
		angularPath.push(map[m0][n0]);
		pathAngular.push(`[${m0}][${n0}]->`);
		n0++;
	}

	while (m0 < m + 1) {
		angularPath.push(map[m0][n0]);
		pathAngular.push(`[${m0}][${n0}]->`);
		m0++;
	}

	pathAngular = pathAngular.join().replace(/,/g, "");
	let fuelAngular = angularPath.length - 1;
	for (let i = 0; i < angularPath.length - 1; i++) {
		fuelAngular += Math.abs(angularPath[i] - angularPath[i + 1]);
	}

	let angularSteps2 = m + n;
	let angularPath2 = [];
	let m02 = 0;
	let n02 = 0;
	let pathAngular2 = [];

	while (m02 < m) {
		angularPath2.push(map[m02][n02]);
		pathAngular2.push(`[${m02}][${n02}]->`);
		m02++;
	}

	while (n02 < n + 1) {
		angularPath2.push(map[m02][n02]);
		pathAngular2.push(`[${m02}][${n02}]->`);
		n02++;
	}

	pathAngular2 = pathAngular2.join().replace(/,/g, "");
	let fuelAngular2 = angularPath2.length - 1;
	for (let i = 0; i < angularPath2.length - 1; i++) {
		fuelAngular2 += Math.abs(angularPath2[i] - angularPath2[i + 1]);
	}

	if (fuelAngular < fuel) {
		roverPath.fuel = fuelAngular;
		roverPath.steps = angularSteps;
		roverPath.path = pathAngular;
	}

	if (fuelAngular2 < fuelAngular) {
		roverPath.fuel = fuelAngular2;
		roverPath.steps = angularSteps2;
		roverPath.path = pathAngular2;
	}

	return roverPath;
}

module.exports = {
	calculateRoverPath,
};
